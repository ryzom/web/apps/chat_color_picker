<?php

/* Copyright (C) 2012 Winch Gate Property Limited
 * 
 * This file is part of ryzom_app.
 * ryzom_app is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ryzom_app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ryzom_app.  If not, see <http://www.gnu.org/licenses/>.
 */

define('APP_NAME', 'app_chat_color_picker');

include_once('../config.php');
include_once(RYAPP_PATH . '/lang.php');

define('APP_PATH', RYAPP_PATH . APP_NAME);
define('APP_URL', RYAPP_URL . APP_NAME . '/');

ryzom_auth_user(true);
//reset_display
$c = '';

if ($ig) {
    $c.="<h2><font color=red>" . _t('not_acces_ig') . "</font></h2>";
} else {
    $c.='<table><tr>';
    $c.='<td>Color Picker:<input class="jscolor {onFineChange:\'update(this)\'}" value="000000"></td>';
    $c.='<td>Chat Color:<input id="rgb" value="@{000F}"><td>';
    $c.='</tr></table>';
}

echo ryzom_app_render(APP_NAME, $c, '', array(
    APP_URL . 'data/jscolor.js',
));
?>
